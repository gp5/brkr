import datetime
from app import app, db
from flask import jsonify, request
from app.views.auth import requires_auth
from app.models.post import Post


@app.route('/createPost', methods=['POST'])
@requires_auth
def create_post():
    body = request.get_json(force=True)
    if 'title' not in body:
        return 'Missing title', 400
    if 'content' not in body:
        return 'Missing content', 400
    title = body['title']
    content = body['content']
    user_id = request.user.user_id
    created_time = datetime.datetime.now()

    post = Post(user_id=user_id, title=title, content=content, created_time=created_time)
    db.session.add(post)
    db.session.commit()

    return jsonify(post.to_dictionary())
