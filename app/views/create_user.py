import datetime
from app import app, db
from flask import jsonify, request
from app.models.user import User


@app.route('/createUser', methods=['POST'])
def create_user():
    body = request.get_json(force=True)
    if 'name' not in body:
        return 'Missing name', 400
    if 'emailId' not in body:
        return 'Missing emailId', 400
    if 'password' not in body:
        return 'Missing password', 400
    name = body['name']
    email_id = body['emailId']
    password = body['password']

    user = User(email_id=email_id, name=name, password=password)
    db.session.add(user)
    db.session.commit()

    return jsonify(user.to_dictionary())
