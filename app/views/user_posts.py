from app import app, db
from flask import jsonify
from app.models.post import Post


@app.route('/posts/<int:user_id>', methods=['GET'])
def get_posts(user_id):
    posts = [p.to_dictionary() for p in Post.query.filter_by(user_id=user_id).order_by(db.desc(Post.created_time))]
    resp = jsonify({
        'posts': posts
    })
    return resp
