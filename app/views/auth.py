from flask import request
from functools import wraps
from app.models.user import User
from app import app


def _check_auth(username, password):
    user = User.query.filter_by(email_id=username).first()
    if not user:
        app.logger.debug('Did not find user: %s', username)
        return False
    if password != user.password:
        app.logger.debug('Password did not match: %s', username)
        return False
    request.user = user
    return True


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not _check_auth(auth.username, auth.password):
            return 'Invalid credentials', 401
        return f(*args, **kwargs)
    return decorated
