from app import db


class Post(db.Model):
    __tablename__ = 'Posts'

    post_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey('Users.user_id'))
    title = db.Column(db.String, nullable=False)
    content = db.Column(db.String, nullable=False)
    created_time = db.Column(db.DateTime, nullable=False)

    def to_dictionary(self):
        return {
            'postId': self.post_id,
            'userId': self.user_id,
            'title': self.title,
            'content': self.content,
            'createdTime': self.created_time.isoformat()
        }

    def __repr__(self):
        return '{} | {}'.format(self.title, self.created_time)
