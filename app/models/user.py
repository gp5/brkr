from app import db


class User(db.Model):
    __tablename__ = 'Users'

    user_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email_id = db.Column(db.String, unique=True, nullable=False)
    password = db.Column(db.String, nullable=False)
    name = db.Column(db.String)
    posts = db.relationship('Post', backref='user', lazy='dynamic')

    def to_dictionary(self):
        return {
            'userId': self.user_id,
            'emailId': self.email_id,
            'password': self.password,
            'name': self.name
        }

    def __repr__(self):
        return '{} | {}'.format(self.email_id, self.name)
