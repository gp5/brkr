import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config.from_object('config')

db = SQLAlchemy(app)

from app import views, models

import logging.handlers
file_handler = logging.handlers.RotatingFileHandler('brkr.log', maxBytes=5 * 1024 * 1024, backupCount=3)
app.logger.addHandler(file_handler)
