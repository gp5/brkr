import os
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'postgresql://localhost:5432/brkr'
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

# I don't really understand what this does, but without it, a warning shows up when running the app
# See http://stackoverflow.com/a/33790196
SQLALCHEMY_TRACK_MODIFICATIONS = False
