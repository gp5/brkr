#!/usr/bin/env python3

import argparse
import json
import requests


def main():
    parser = argparse.ArgumentParser('Lists all posts made by a user')
    parser.add_argument('-u', '--user-id', type=int, required=True, help='User Id of user whose posts to list')
    parser.add_argument('--hostname', default='http://localhost:5000', help='Hostname to target')
    args = parser.parse_args()

    url = args.hostname
    if not url.endswith('/'):
        url += '/'
    url += 'posts/' + str(args.user_id)

    r = requests.get(url)
    if r.status_code != requests.codes.ok:
        print('Error:', r.status_code)
        return
    print(json.dumps(r.json(), indent=4, sort_keys=True))


if __name__ == "__main__":
    main()
