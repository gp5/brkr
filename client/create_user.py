#!/usr/bin/env python3

import argparse
import json
import requests
import getpass


def main():
    parser = argparse.ArgumentParser('Creates a user')
    parser.add_argument('-e', '--email-id', required=True, help='Email Id')
    parser.add_argument('-n', '--name', required=True, help='Name')
    parser.add_argument('--hostname', default='http://localhost:5000', help='Hostname to target')
    args = parser.parse_args()

    email_id = args.email_id
    name = args.name
    password = getpass.getpass('Enter password for ' + name + ': ')

    url = args.hostname
    if not url.endswith('/'):
        url += '/'
    url += 'createUser'

    body = json.dumps({
        'emailId': email_id,
        'name': name,
        'password': password
    })

    r = requests.post(url, data=body)
    if r.status_code != requests.codes.ok:
        print('Error:', r.status_code)
        return
    print(r.json())


if __name__ == "__main__":
    main()
