#!/usr/bin/env python3

import argparse
import json
import requests
import getpass


def main():
    parser = argparse.ArgumentParser('Creates a post')
    parser.add_argument('-e', '--email-id', required=True, help='Email Id')
    parser.add_argument('-t', '--title', required=True, help='Title of post')
    parser.add_argument('-c', '--content', required=True, help='Path to file containing content')
    parser.add_argument('--hostname', default='http://localhost:5000', help='Hostname to target')
    args = parser.parse_args()

    email_id = args.email_id
    password = getpass.getpass('Enter password for ' + email_id + ' : ')

    url = args.hostname
    if not url.endswith('/'):
        url += '/'
    url += 'createPost'

    auth = requests.auth.HTTPBasicAuth(email_id, password)

    with open(args.content) as fin:
        body = json.dumps({
            'title': args.title,
            'content': fin.read()
        })

    r = requests.post(url, data=body, auth=auth)
    if r.status_code != requests.codes.ok:
        print('Error:', r.status_code)
        return
    print(r.json())


if __name__ == "__main__":
    main()
